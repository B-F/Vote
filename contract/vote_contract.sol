pragma solidity 0.4.21;
contract Vote_contract{
    address public owner;   ///Publisher
    Person[20] private persons; ///Max 20 Persons
    uint64 private usedSlots;
    /// Vote data type
    struct Person {
        address _name;
        uint64 _votes;
        bool _used;
    }
    
    /// Constructor
    function Vote_contract() public {
      owner = msg.sender;   ///Set owner
      usedSlots=0;
   }
   
   /// Destroy contract
   function kill() public {
      if(msg.sender == owner) selfdestruct(owner);
   }
   
   ///Check if a person already exists
   function checkPersonExists(address name) public constant returns(bool){
       for(uint256 i = 0; i < persons.length; i++){
         if((persons[i]._name == name) && (persons[i]._used == true)) return true;
       }
       return false;
   }
   
   /// Vote for an object or creat a new vote if necessary
   function vote(address name) public {
       uint64 i;
       if(checkPersonExists(name))
       {
         //Increase votes
         for(i = 0; i < persons.length; i++){
            if(persons[i]._name == name) persons[i]._votes++;
         }
       }
       else
       {
          require(usedSlots<=20);       
          //Create new person
          for(i = 0; i < persons.length; i++){
              if(persons[i]._used == false)
              {
                  persons[i]._name = name;
                  persons[i]._votes = 1;
                  persons[i]._used = true;
                  usedSlots++;
                  break;
              }
          }
       }
   }
   
   /// Delete a person from the persons list
   function deletePerson(address name) private {
       for(uint64 i = 0; i < persons.length; i++){
         if(persons[i]._name == name){
              if(persons[i]._used == true)
              {
                  persons[i]._used = false;
                  usedSlots--;
                  break;
              }
         }
       }
   }
   
   function reset() public {
        for(uint64 i = 0; i < persons.length; i++){
              if(persons[i]._used == true){
                deletePerson(persons[i]._name);
              }
       }
   }
   
   /// Get the number of votes from a person
   function getVotes(address name) public constant returns(uint64){
        require(checkPersonExists(name));
        for(uint64 i = 0; i < persons.length; i++){
         if(persons[i]._name == name) return persons[i]._votes;
       }
   }
   
   /// Get adress of a person (that is on the vote list)
   function getPersonFromList(uint32 pos) public constant returns(address){
       uint64 countSlots = 1;
       uint64 i;
       for(i = 0; i < persons.length; i++){
              if(persons[i]._used == true){
                if(countSlots==pos){
                    return persons[i]._name;
                    break;   
                }
                else{
                    countSlots++;   
                }
              }
       }
   }
   
   /// Get number of active persons on vote list
   function getPersonsCount() public constant returns(uint64){
       uint64 countSlots = 0;
       uint64 i;
       for(i = 0; i < persons.length; i++){
              if(persons[i]._used == true){
                countSlots++;
              }
       }
       return countSlots;
   }
   
   /// Get number of all votes
   function getVoteCount() public constant returns(uint64){
       uint64 countVotes =0;
       uint64 i;
       for(i = 0; i < persons.length; i++){
              if(persons[i]._used == true){
                countVotes=persons[i]._votes+countVotes;
              }
       }
       return countVotes;
   }  

}