const path = require('path')

module.exports = {
   entry: path.join(__dirname, 'src/js', 'index.js'), //Frontend located in src folder
   output: {
      path: path.join(__dirname, 'dist'),
      filename: 'build.js' // Packed js script in build.js file
   },
   module: {
      loaders: [{
         test: /\.css$/, // Load css
         use: ['style-loader', 'css-loader'],
         include: /src/
      }, {
         test: /\.jsx?$/, // Load js and jsx files
         loader: 'babel-loader',
         exclude: /node_modules/,
         query: {
            presets: ['es2015', 'react', 'stage-2']
         }
      }, {
         test: /\.json$/, // Load json
         loader: 'json-loader'
      }]
   }
}
