import React from 'react'
import ReactDOM from 'react-dom'
import Web3 from 'web3'
import './../css/index.css'

import {DemoApp} from './../js/DemoApp.js'

ReactDOM.render(
   <DemoApp />,
   document.querySelector('#root')
)