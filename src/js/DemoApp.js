import React from 'react'
import ReactDOM from 'react-dom'
import Web3 from 'web3'
import './../css/DemoApp.css'
import {Bootstrap, Grid, Row, Col, ButtonGroup, Button, Glyphicon, bsStyle, Jumbotron, PageHeader } from 'react-bootstrap'

export class DemoApp extends React.Component
{
  constructor(props)
  {
     super(props)
     this.state =
     {
        Readonly: true,
        Balance:"Sync...",
        UserAdress:"Sync...",
        PersonsCount:"Sync...",
        VotesCount:"Sync...",
        CountDown: 30,
        Persons:[],
        Votes:[],
     }

     //Connection to Metamask or HttpProvider
     if(typeof web3 != 'undefined')
     {
        console.log("Web 3 injected (Metamask or external provider found)")
        this.web3 = new Web3(web3.currentProvider)

        //SMART CONTRACT ABI
        const MyContract = web3.eth.contract([{"constant":false,"inputs":[],"name":"kill","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[],"name":"reset","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"inputs":[],"payable":false,"stateMutability":"nonpayable","type":"constructor"},{"constant":false,"inputs":[{"name":"name","type":"address"}],"name":"vote","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"name":"name","type":"address"}],"name":"checkPersonExists","outputs":[{"name":"","type":"bool"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"pos","type":"uint32"}],"name":"getPersonFromList","outputs":[{"name":"","type":"address"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"getPersonsCount","outputs":[{"name":"","type":"uint64"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"getVoteCount","outputs":[{"name":"","type":"uint64"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"name","type":"address"}],"name":"getVotes","outputs":[{"name":"","type":"uint64"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"owner","outputs":[{"name":"","type":"address"}],"payable":false,"stateMutability":"view","type":"function"}])
        this.state.ContractInstance = MyContract.at("0x7359960d4afc39568549a3b363f8d444ffa6f916")
        this.state.Readonly=false;
     }
     else
     {
        console.log("Web3 object not injected. Using HTTP provider Consider using Metamask http://truffleframework.com/tutorials/truffle-and-metamask");
        //this.web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:8545"))
        //this.web3 = new Web3(new Web3.providers.HttpProvider("https://mainnet.infura.io/noapikey"));
        alert("No web3 provider detected. Using HttpProvider (connecting to ropsten testnet) as fallback. Switched to read-only mode. Consider using Metamask https://metamask.io/ to use vote functions.")
        this.web3 = new Web3(new Web3.providers.HttpProvider("https://ropsten.infura.io"));

        //SMART CONTRACT ABI
        const MyContract = this.web3.eth.contract([{"constant":false,"inputs":[],"name":"kill","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[],"name":"reset","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"inputs":[],"payable":false,"stateMutability":"nonpayable","type":"constructor"},{"constant":false,"inputs":[{"name":"name","type":"address"}],"name":"vote","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"name":"name","type":"address"}],"name":"checkPersonExists","outputs":[{"name":"","type":"bool"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"pos","type":"uint32"}],"name":"getPersonFromList","outputs":[{"name":"","type":"address"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"getPersonsCount","outputs":[{"name":"","type":"uint64"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"getVoteCount","outputs":[{"name":"","type":"uint64"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"name","type":"address"}],"name":"getVotes","outputs":[{"name":"","type":"uint64"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"owner","outputs":[{"name":"","type":"address"}],"payable":false,"stateMutability":"view","type":"function"}])
        this.state.ContractInstance = MyContract.at("0x7359960d4afc39568549a3b363f8d444ffa6f916")
        this.state.Readonly=true;
     }
	 
     window.a = this.state
   }

  //Update wallet balance
  UpdateBalance()
  {
    let hlpthis = this;
    var address =this.web3.eth.accounts[0]

    try {
		//Call contract function
        this.web3.eth.getBalance(address, function (error, wei)
        {
            if (!error) {
              hlpthis.setState({
                 Balance: parseFloat(web3.fromWei(wei, 'ether')) + "  ETH"
              })
            }
        });
    } catch (err) {
      if(hlpthis.state.Readonly==true)
      {
        hlpthis.setState({
           Balance: "No wallet..."
        })
      }
      else {
        hlpthis.setState({
           Balance: "Sync..."
        })
      }
    }
  }

  //Update persons counter
  UpdatePersonsCount()
  {
    let hlpthis = this;
	//Call contract function
    this.state.ContractInstance.getPersonsCount((err, result) => {
            if(hlpthis.state.PersonsCount!=result.c[0])
            {
            hlpthis.setState({
               PersonsCount: result.c[0]
            })
            hlpthis.UpdatePersons()
            }
       })
  }

  //Update vote counter
  UpdateVotesCount()
  {
    let hlpthis = this;
	//Call contract function
    this.state.ContractInstance.getVoteCount((err, result) => {
            if(hlpthis.state.VotesCount!=result.c[0])
            {
                hlpthis.setState({
                   VotesCount: result.c[0]
                })
                for(var i=0;i<hlpthis.state.Votes.length;i++)  {
                  hlpthis.UpdateVotes(i);
                }

            }
       })
  }

  //Append to list (used to display data in diagram)
  appendToLists(item)
  {
    this.state.Persons.push(item);
    this.state.Votes.push(0);
    this.UpdateVotes(this.state.Votes.length-1);
  }

  //Update Persons Array
  UpdatePersons()
  {
    let hlpthis = this;
    this.setState({
       Persons: [],
       Votes: [],
    })

    for(var i = 1; i <= hlpthis.state.PersonsCount; i++)
    {
	  //Call contract function
      this.state.ContractInstance.getPersonFromList(i,(err, result) => {
                hlpthis.appendToLists(result);
        })
    }
  }
  
  //Update Votes Array
  UpdateVotes(pos)
  {
    let hlpthis = this;
	//Call contract function
    this.state.ContractInstance.getVotes(hlpthis.state.Persons[pos],(err, result) => {
              hlpthis.state.Votes[pos]=result.c[0];
       })
  }

  //Update User Wallet Adress
  UpdateUserAdress()
  {
    if(this.state.Readonly==true)
    {
      this.setState({
         UserAddress: "No wallet..."
      })
    }
    else {
      this.setState({
         UserAddress: this.web3.eth.accounts[0]
      })
    }

  }

  //Display refresh countdown
  UpdateCountDown()
  {
    var val= this.state.CountDown;
    val--;
      this.setState({
         CountDown: val
      })

  }

  //Start Timers and set timer expired events aber UI is loaded
  componentDidMount(){
    this.UpdateStates()
    setInterval(this.UpdateStates.bind(this), 30e3)
    setInterval(this.UpdateCountDown.bind(this), 1e3)

  }

 //Update all data displayed in the UI (get changes from blockchain)
 UpdateStates()
 {
   this.setState({
      CountDown: 30		//30 sec interval
   })

   this.UpdateUserAdress()
   this.UpdateBalance()
   this.UpdatePersonsCount()
   this.UpdateVotesCount()
 }

 //Display activ election (data stored in Persons + Votes array)
 DrawChart()
 {
   if((this.state.PersonsCount >= 0)&&(this.state.PersonsCount==this.state.Votes.length)&&(this.state.PersonsCount==this.state.Persons.length))
   {
   var BarChart = require("react-chartjs").Bar;
   var xData=[];
   var yData=[];
   for( var i = 0; i< this.state.PersonsCount; i++)
   {
     xData.push(this.state.Persons[i]);
     yData.push(this.state.Votes[i]);
   }

         const chartData = {
         	labels: xData,
         	datasets: [
         		{
         			label: "# of Votes",
              pointHoverRadius: 5,
         			fillColor: "rgba(17,0,255,0.5)",
         			strokeColor: "rgba(220,220,220,0.8)",
         			highlightFill: "rgba(220,220,220,0.75)",
         			highlightStroke: "rgba(220,220,220,1)",
         			data: yData
         		},
         	]
         };
         const options ={
           responsive: false,
           maintainAspectRatio: true,
           responsiveAnimationDuration: 500,
         }

         return(
           <BarChart  data={chartData} options={options} height="500" width="500"/>
         )
    }
    else {
       return(<p>Sync chart....</p>)
    }
 }

 //Click event handler
 HandleVote(evt)
 {
   //Vote-Button
   if(evt.target.id=="voteBtn")
   {
     var input = document.getElementById("VoteInputBox").value
	 //Vote for new person
	 //Call contract function
     this.state.ContractInstance.vote(input,(err, result) => {
       })
   }
   //Reset-Button
   else if(evt.target.id=="deleteBtn")
   {
	 //Reset election
	 //Call contract function
     document.getElementById("VoteInputBox").value=""
     this.state.ContractInstance.reset((err, result) => {
       })
   }
   //Vote-Buttons for existing persons
   else if(this.state.Persons.length>evt.target.id)
   {
	 //Vote for existing person
	 //Call contract function
     this.state.ContractInstance.vote(this.state.Persons[evt.target.id],(err, result) => {
       })
   }
 }

 //Create vote buttons for known persons participating in election
 DrawVoteList()
 {
   var buttons = [];
   /// Wait for async functions
   if(this.state.Persons.length >= this.state.PersonsCount)
   {
       for (var i = 0; i < this.state.PersonsCount; i++) {
          buttons.push( <Button bsStyle="primary" disabled={this.state.Readonly} href="#" id={i} onClick={this.HandleVote.bind(this)}><Glyphicon glyph="glyphicon glyphicon-thumbs-up" />      {this.state.Persons[i]}</Button>);
       }
    }
    else
    {
      buttons.push(<p>Sync...</p>);
    }
   return (<div><ButtonGroup vertical block>{buttons}</ButtonGroup></div>);
}
  //Create / Update complete application UI -> React only re-renders elements when corresponding data has changed
 render()
 {
   const wallpaper = '/media/black_background.jpg';
   const JumboStyles = {
       background: 'black'
   };
   const TextStyles = {
       color: 'white'
     };

   return(
     <div className="main-container">
     <Jumbotron style={JumboStyles}>
      <div className="main-container">
       <PageHeader style={TextStyles}>Vote! - A sample dApp</PageHeader>
       <span style={TextStyles}>Author: Bauer Florian</span>
       </div>
     </Jumbotron>
     <Jumbotron>
     <div className="main-container">
       <div>
           <b>Connected wallet:</b><br/>
           <span> {this.state.UserAddress}</span>
           <br/>
           <b>Balance:</b><br/>
           <span> {this.state.Balance}</span>
      </div>
      <div>
            <br/>
            <b>Vote by selecting an existing person or enter a new one!</b>
            <br/><br/>
            {this.DrawVoteList()}
            <br/>
            <form className="form-horizontal">
                <input type="text" disabled={this.state.Readonly} defaultValue="0x7359960d4afc39568549a3b363f8d444ffa6f916" className="form-control" ref={(c) => this.title = c} id="VoteInputBox" />
            </form>
            <Button bsStyle="success" type="button" disabled={this.state.Readonly} id="voteBtn" onClick={this.HandleVote.bind(this)}>       Vote      </Button><br/><br/>
            <Button bsStyle="danger" type="button" disabled={this.state.Readonly} id="deleteBtn" onClick={this.HandleVote.bind(this)} >Restart election</Button>
      </div>
      <div>
            <br/>
            <em><b>Results (next refresh in: {this.state.CountDown})</b></em>
            <br/><br/>
            <b>Number of persons part of election:</b><br/>
            <span> {this.state.PersonsCount}</span><br/>
            <b>Total votes:</b><br/>
            <span> {this.state.VotesCount}</span><br/>
            <br/><br/>
            {this.DrawChart()}
      </div>
    </div>
    </Jumbotron>
    </div>
  );
 }
}
